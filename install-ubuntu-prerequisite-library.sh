#!/bin/bash
echo "Start base tools for ubuntu"

echo "Update your Ubuntu 20 system."
apt-get -y update && sudo apt-get -y dist-upgrade

echo "Installing zip unzip"
apt install -y zip unzip

echo "Installing docker-ce"
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu  $(lsb_release -cs)  stable"
sudo apt update -y
apt-cache policy docker-ce
sudo apt install -y docker-ce docker-ce-cli containerd.io
sudo systemctl enable docker
sudo systemctl start docker

echo "Installing docker-compose"
sudo curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

echo "End base tools for ubuntu"