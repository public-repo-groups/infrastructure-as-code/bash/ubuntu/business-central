#!/bin/bash
echo "Start run_install_config"

echo "Execute install-ubuntu-prerequisite-library.sh"
./install-ubuntu-prerequisite-library.sh

echo "Execute build-image-business-central.sh"
./build-image-business-central.sh

echo "End run_install_config"
